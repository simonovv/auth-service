import Sequelize from 'sequelize';

const config = require('../config/database')[process.env.NODE_ENV || 'development'];

const sequelize = new Sequelize(config.DB_NAME, config.DB_USER, config.DB_PASSWORD, {
  dialect: config.DIALECT,
  port: config.PORT,
  host: config.HOST,
});

export default sequelize;
