import Sequelize from 'sequelize';
import sequelize from '../helpers/database';

const user = sequelize.define('user', {
  id: {
    type: Sequelize.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  },
  email: {
    type: Sequelize.STRING(25),
    unique: {
      msg: 'This email already exists',
    },
    validate: {
      min: 5,
      max: 25,
      isEmail: true,
    },
  },
  username: {
    type: Sequelize.STRING,
    unique: true,
  },
  password: {
    type: Sequelize.STRING,
  },
  token: {
    type: Sequelize.STRING,
  },
}, { timestamps: false });

export default user;
