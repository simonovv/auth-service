import jsonwebtoken from 'jsonwebtoken';
import secret from '../../../config/server';

const getToken = async (email) => {
  const token = await jsonwebtoken.sign(email, secret.SECRET);
  return token;
};

const verifyToken = (token) => { jsonwebtoken.verify(token, secret.SECRET); };

export default {
  getToken,
  verifyToken,
};
