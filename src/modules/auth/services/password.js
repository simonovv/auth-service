import bcrypt from 'bcryptjs';
import conf from '../../../config/server';

const hashPassword = async (password) => {
  const salt = await bcrypt.genSalt(conf.SALT_I);
  const hash = await bcrypt.hash(password, salt);
  return hash;
};

const comparePassword = async (password, existPassword) => {
  const isMatch = await bcrypt.compare(password, existPassword);
  return isMatch;
};

export default {
  hashPassword,
  comparePassword,
};
