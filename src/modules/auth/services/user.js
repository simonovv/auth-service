import User from '../../../models/user';

const getAllUsers = async (req) => {
  const { sort, by } = req.query;
  if (sort) {
    const users = await User.findAll({
      order: [
        [sort, 'DESC'],
      ],
    })
      .catch(() => Promise.reject(new Error('No such field to sort')));
    return users;
  }
  if (sort && (by === 'ASC' || by === 'DESC')) {
    const users = await User.findAll({
      order: [
        [sort, by],
      ],
    });
    return users;
  }
  const users = await User.findAll();
  return users;
};

const getUserById = async (req) => {
  const { id } = req.body.params;

  const user = await User.findOne({
    where: { id },
  });

  if (!user) {
    return Promise.reject(new Error('User with this id doesnt exists'));
  }

  return user;
};

const updateUser = async (req) => {
  const { user } = req.body;

  const newUser = await User.update(user)
    .catch(() => Promise.reject(new Error('Error occured while processing database update')));

  return newUser;
};

export default {
  getAllUsers,
  getUserById,
  updateUser,
};
