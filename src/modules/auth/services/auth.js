import User from '../../../models/user';
import jwt from './jwtoken';
import passwordService from './password';

const createUser = async (req) => {
  const { email, password } = req.body;
  const user = await User.findOne({
    where: { email },
  });

  let createdUser;
  if (!user) {
    const token = await jwt.getToken(email);
    createdUser = await User.create({
      email,
      username: email.split('@')[0],
      password: await passwordService.hashPassword(password),
      token,
    });
    return createdUser.dataValues;
  }
  return Promise.reject(new Error('E-mail already exists'));
};

const findUser = async (req) => {
  const { email, password } = req.body;
  const user = await User.findOne({
    where: { email },
  });

  if (!user) {
    return Promise.reject(new Error('User with this e-mail doesnt exists'));
  }

  const valid = await passwordService.comparePassword(password, user.password);

  return valid ? user : Promise.reject(new Error('Password is incorect'));
};

export default {
  createUser,
  findUser,
};
