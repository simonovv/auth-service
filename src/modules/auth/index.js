import express from 'express';
import authController from './controllers/authController';
import userController from './controllers/userController';

const router = express.Router();

router.route('/user/signup')
  .post(authController.signUp);

router.route('/user/signin')
  .post(authController.signIn);

router.route('/users')
  .get(userController.getAllUsers);

router.route('/user/:id')
  .get(userController.getUserById)
  .put();

export default router;
