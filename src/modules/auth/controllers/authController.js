import authService from '../services/auth';

const signUp = async (req, res) => {
  const user = await authService.createUser(req)
    .catch(err => res.status(409).send({ error: err.message }));
  res.status(200).send(user);
};

const signIn = async (req, res) => {
  const user = await authService.findUser(req)
    .catch(err => res.status(409).send({ error: err.message }));
  res.status(200).send(user);
};

export default {
  signUp,
  signIn,
};
