import userService from '../services/user';

const getAllUsers = async (req, res) => {
  const users = await userService.getAllUsers(req)
    .catch(err => res.status(309).send({ error: err.message }));
  res.status(200).send(users);
};

const getUserById = async (req, res) => {
  const user = await userService.getUserById(req)
    .catch(err => res.status(309).send({ error: err }));
  res.status(200).send(user);
};

const updateUser = async (req, res) => {
  const user = await userService.updateUser(req)
    .catch(err => res.status(309).send({ error: err }));
  res.status(200).send(user);
};

export default {
  getAllUsers,
  getUserById,
  updateUser,
};
