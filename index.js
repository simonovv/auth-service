import express from 'express';
import bodyParser from 'body-parser';
import routers from './src/modules/index';
import sequlize from './src/helpers/database';

import serverConf from './src/config/server';

const app = express();

sequlize.sync();

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'POST,GET,OPTIONS,PUT,DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Accept');
  next();
});

app.use(bodyParser.json());
app.use(routers);

app.listen(process.env.PORT || serverConf.PORT, () => {
  console.log(`Server running on port ${process.env.PORT || serverConf.PORT}`);
});
