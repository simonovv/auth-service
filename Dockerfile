FROM node:8-alpine

WORKDIR /app

COPY package.json ./

RUN npm install

COPY . .

EXPOSE 8080

CMD ['nodemon', ['./src/index.js']]